package de.haddel.controller;

import de.haddel.calculator.Calculator;
import de.haddel.frontend.Frontend;

/**
 * Created by Doerner_Ju on 04.12.2015.
 */
public class Controller {

    Frontend frontend;
    Calculator calculator;

    public Controller() {

        frontend = new Frontend("Calculator", this);
        calculator = new Calculator();

    }

    public void startCalculation() {
        frontend.setOutput(calculator.calculate(frontend.getInput()));
    }


}
