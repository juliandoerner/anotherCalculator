package de.haddel.calculator.exceptions;

public class EvaluationException extends RuntimeException {
    public EvaluationException(String msg) {
        super(msg);
    }
}
