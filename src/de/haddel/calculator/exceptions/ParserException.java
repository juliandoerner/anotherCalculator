package de.haddel.calculator.exceptions;

public class ParserException extends RuntimeException {
    public ParserException(String msg) {
        super(msg);
    }
}