package de.haddel.calculator.tokenizer;

public class Token {

  // Anfang Attribute
    public static final int EPSILON = 0;
    public static final int PLUSMINUS = 1;
    public static final int MULTDIV = 2;
    public static final int RAISED = 3;
    public static final int FUNCTION = 4;
    public static final int OPEN_B = 5;
    public static final int CLOSED_B = 6;
    public static final int NUMBER = 7;
    public static final int VARIABLE = 8;

    public final int token;
    public final String sequence;
  // Ende Attribute


    public Token(int token, String sequence) {
        super();
        this.token = token;
        this.sequence = sequence;
    }
  // Anfang Methoden
  // Ende Methoden
}
