package de.haddel.calculator;

import de.haddel.calculator.exceptions.EvaluationException;
import de.haddel.calculator.exceptions.ParserException;
import de.haddel.calculator.nodes.ExpressionNode;
import de.haddel.calculator.nodes.FunctionExpressionNode;
import de.haddel.calculator.parser.Parser;
import de.haddel.calculator.parser.SetVariable;
import de.haddel.calculator.tokenizer.Tokenizer;

public class Calculator {

    Tokenizer tokenizer;
    Parser parser;


    public Calculator() {

        tokenizer = new Tokenizer();

        parser = new Parser();


        tokenizer.add("\\+|-", 1);
        tokenizer.add("\\*|/", 2);
        tokenizer.add("\\^", 3);
        tokenizer.add(FunctionExpressionNode.giveAllFunctions(), 4);
        tokenizer.add("\\(", 5);
        tokenizer.add("\\)", 6);
        tokenizer.add("[0-9.]+", 7);
        tokenizer.add("[a-zA-Z][a-zA-Z0-9_]*", 8);
    }

    public String calculate(String expression) {

        ExpressionNode rootExpression = null;

        try {

            tokenizer.tokenize(expression);

            rootExpression = parser.parse(tokenizer.getTokens());
            rootExpression.accept(new SetVariable("pi", Math.PI));


        } catch (ParserException exception) {
            System.err.println(exception.getMessage());
        } catch (EvaluationException exception) {
            System.out.println(exception.getMessage());
        }

        return String.valueOf(rootExpression.getValue());
    }
}
