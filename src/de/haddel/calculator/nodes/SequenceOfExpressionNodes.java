package de.haddel.calculator.nodes;

import java.util.LinkedList;

public abstract class SequenceOfExpressionNodes implements ExpressionNode {

    LinkedList<Term> terms;

    public SequenceOfExpressionNodes() {
        this.terms = new LinkedList<Term>();
    }

    public SequenceOfExpressionNodes(ExpressionNode expression, boolean isPositive) {
        this.terms = new LinkedList<Term>();
        this.terms.add(new Term(isPositive, expression));

    }

    public void add(ExpressionNode expression, boolean isPositive) {
        this.terms.add(new Term(isPositive, expression));
    }


    public class Term {

        public boolean isPositive;
        public ExpressionNode expression;

        public Term(boolean isPositive, ExpressionNode expression) {
            super();
            this.isPositive = isPositive;
            this.expression = expression;
        }
    }
}
