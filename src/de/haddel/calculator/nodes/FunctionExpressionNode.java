package de.haddel.calculator.nodes;

import de.haddel.calculator.exceptions.EvaluationException;

public class FunctionExpressionNode implements ExpressionNode {

    public static final int FUNCTION_SIN = 1;
    public static final int FUNCTION_COS = 2;
    public static final int FUNCTION_TAN = 3;

    public static final int FUNCTION_ASIN = 4;
    public static final int FUNCTION_ACOS = 5;
    public static final int FUNCTION_ATAN = 6;

    public static final int FUNCTION_SQRT = 7;

    private int actualFunction;
    private ExpressionNode argument;


    public FunctionExpressionNode(int actualFunction, ExpressionNode argument) {
        super();

        this.actualFunction = actualFunction;
        this.argument = argument;
    }

    public static int stringToFunctionID(String functionString) {
        if (functionString.equals("sin"))
            return FUNCTION_SIN;
        else if (functionString.equals("cos"))
            return FUNCTION_COS;
        else if (functionString.equals("tan"))
            return FUNCTION_TAN;

        else if (functionString.equals("asin"))
            return FUNCTION_ASIN;
        else if (functionString.equals("acos"))
            return FUNCTION_ACOS;
        else if (functionString.equals("atan"))
            return FUNCTION_ATAN;

        if (functionString.equals("sqrt"))
            return FUNCTION_SQRT;

        throw new EvaluationException("Falsche Funktion: " + functionString);
    }

    public static String giveAllFunctions() {
        return "sin|cos|tan|asin|acos|atan|sqrt";
    }

    public int getType() {
        return ExpressionNode.FUNCTION_NODE;
    }

    public double getValue() {
        switch (actualFunction) {
            case FUNCTION_SIN:
                return Math.sin(Math.toRadians(argument.getValue()));
            case FUNCTION_COS:
                return Math.cos(Math.toRadians(argument.getValue()));
            case FUNCTION_TAN:
                return Math.tan(Math.toRadians(argument.getValue()));
            case FUNCTION_ASIN:
                return Math.toDegrees(Math.asin(argument.getValue()));
            case FUNCTION_ACOS:
                return Math.toDegrees(Math.acos(argument.getValue()));
            case FUNCTION_ATAN:
                return Math.toDegrees(Math.atan(argument.getValue()));

            case FUNCTION_SQRT:
                return Math.sqrt(argument.getValue());
        }
        throw new EvaluationException("Falsche Funktions ID: " + actualFunction);
    }

    public void accept(ExpressionNodeViewer viewer) {
        viewer.view(this);

        argument.accept(viewer);
    }
}
