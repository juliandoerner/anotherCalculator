package de.haddel.calculator.nodes;

public class AdditionExpressionNode extends SequenceOfExpressionNodes {


    public AdditionExpressionNode() {
        super();
    }

    public AdditionExpressionNode(ExpressionNode expression, boolean isPositive) {
        super(expression, isPositive);
    }

    public int getType() {
        return ExpressionNode.ADDITION_NODE;
    }

    public double getValue() {
        double sum = 0.0;

        for (Term actualTerm : terms) {
            if (actualTerm.isPositive) {
                sum += actualTerm.expression.getValue();
            } else {
                sum -= actualTerm.expression.getValue();
            }
        }

        return sum;
    }

    public void accept(ExpressionNodeViewer viewer) {
        viewer.view(this);

        for (Term actualTerm : terms)
            actualTerm.expression.accept(viewer);
    }
}
