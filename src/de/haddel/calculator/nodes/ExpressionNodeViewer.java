package de.haddel.calculator.nodes;

public interface ExpressionNodeViewer {

    public void view(VariableExpressionNode expressionNode);
    public void view(ConstantExpressionNode expressionNode);
    public void view(AdditionExpressionNode expressionNode);
    public void view(MultiplicationExpressionNode expressionNode);
    public void view(ExponentiationExpressionNode expressionNode);
    public void view(FunctionExpressionNode expressionNode);

}
