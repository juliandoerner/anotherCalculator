package de.haddel.calculator.nodes;

public class ConstantExpressionNode implements ExpressionNode {

    private double value;

    public ConstantExpressionNode(double value) {
        this.value = value;
    }

    public ConstantExpressionNode(String value) {
        this.value = Double.parseDouble(value);
    }


    public int getType() {
        return ExpressionNode.CONSTANT_NODE;
    }

    public double getValue() {
        return value;
    }

    public void accept(ExpressionNodeViewer viewer) {
        viewer.view(this);
    }
}
