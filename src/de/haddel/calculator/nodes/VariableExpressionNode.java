package de.haddel.calculator.nodes;

import de.haddel.calculator.exceptions.EvaluationException;

public class VariableExpressionNode implements ExpressionNode {

    private String variable;
    private Double value;
    private Boolean valueSet;

    public VariableExpressionNode(String variable) {
        this.variable = variable;
    }


    public int getType() {
        return VARIABLE_NODE;
    }


    public double getValue() {

        if (valueSet)
            return value;
        else
            throw new EvaluationException("Variable: '" + variable + "' ist nicht deklariert");

    }

    public void setValue(Double value) {
        this.value = value;
        this.valueSet = true;
    }

    public void accept(ExpressionNodeViewer viewer) {
        viewer.view(this);
    }

    public String getName(){
        return variable;
    }
}
