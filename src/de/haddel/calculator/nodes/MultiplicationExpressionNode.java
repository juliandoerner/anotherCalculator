package de.haddel.calculator.nodes;

public class MultiplicationExpressionNode extends SequenceOfExpressionNodes {

    public MultiplicationExpressionNode() {
        super();
    }

    public MultiplicationExpressionNode(ExpressionNode expression, boolean isPositive) {
        super(expression, isPositive);
    }


    public int getType() {
        return ExpressionNode.MULTIPLICATION_NODE;
    }

    public double getValue() {
        double product = terms.pop().expression.getValue();

        for (Term actualTerm : terms) {
            if (actualTerm.isPositive) {
                product = product * actualTerm.expression.getValue();
            } else {
                product /= actualTerm.expression.getValue();
            }
        }
        return product;
    }

    public void accept(ExpressionNodeViewer viewer) {
        viewer.view(this);

        for (Term actualTerm : terms)
            actualTerm.expression.accept(viewer);
    }
}
