package de.haddel.calculator.parser;

import de.haddel.calculator.nodes.*;

public class SetVariable implements ExpressionNodeViewer {

    private String variable;
    private Double value;

    public SetVariable(String variable, Double value) {
        super();
        this.value = value;
        this.variable = variable;
    }



    public void view(VariableExpressionNode expressionNode) {

        if (expressionNode.getName().equals(variable))
            expressionNode.setValue(value);

    }


    public void view(ConstantExpressionNode expressionNode) {

    }



    public void view(AdditionExpressionNode expressionNode) {

    }



    public void view(MultiplicationExpressionNode expressionNode) {

    }



    public void view(ExponentiationExpressionNode expressionNode) {

    }



    public void view(FunctionExpressionNode expressionNode) {

    }
}
