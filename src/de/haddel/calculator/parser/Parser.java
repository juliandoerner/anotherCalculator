package de.haddel.calculator.parser;

import de.haddel.calculator.exceptions.ParserException;
import de.haddel.calculator.nodes.*;
import de.haddel.calculator.tokenizer.Token;

import java.util.LinkedList;
import java.util.List;

import static de.haddel.calculator.nodes.FunctionExpressionNode.stringToFunctionID;

public class Parser {

    LinkedList<Token> tokens;
    Token nextToken;

    public ExpressionNode parse(List<Token> tokens) {

        this.tokens = (LinkedList<Token>) tokens;
        nextToken = this.tokens.getFirst();

        ExpressionNode expression = expression();

        if (nextToken.token != Token.EPSILON)
            throw new ParserException("Unerlaubtes Zeichen gefunden: " + nextToken.sequence);

        return expression;
    }

    private void setNextToken() {
        this.tokens.pop();

        if (this.tokens.isEmpty())
            this.nextToken = new Token(Token.EPSILON, "");
        else
            this.nextToken = tokens.getFirst();
    }

    //**********
    //Rekursive Descend Methods, starting by expression in method parse()
    //**********
    private ExpressionNode expression() {

        ExpressionNode expression = signed_term();
        return sum_op(expression);
    }

    private ExpressionNode sum_op(ExpressionNode expr) {

        if (nextToken.token == Token.PLUSMINUS) {
            AdditionExpressionNode sumExpression;

            if (expr.getType() == ExpressionNode.ADDITION_NODE)
                sumExpression = (AdditionExpressionNode) expr;
            else
                sumExpression = new AdditionExpressionNode(expr, true);

            boolean isPositive = nextToken.sequence.equals("+");
            setNextToken();

            ExpressionNode expression = term();
            sumExpression.add(expression, isPositive);

            return sum_op(sumExpression);
        } else {
            return expr;
        }

    }

    private ExpressionNode signed_term() {

        if (nextToken.token == Token.PLUSMINUS) {
            boolean isPositive = nextToken.sequence.equals("+");
            setNextToken();
            ExpressionNode expression = term();

            if (isPositive)
                return expression;
            else
                return new AdditionExpressionNode(expression, false);

        } else {
            return term();
        }

    }

    private ExpressionNode term() {

        ExpressionNode expression = factor();
        return term_op(expression);

    }

    private ExpressionNode term_op(ExpressionNode expr) {

        if (nextToken.token == Token.MULTDIV) {
            MultiplicationExpressionNode multExpression;

            if (expr.getType() == ExpressionNode.MULTIPLICATION_NODE)
                multExpression = (MultiplicationExpressionNode) expr;
            else
                multExpression = new MultiplicationExpressionNode(expr, true);

            boolean isPositive = nextToken.sequence.equals("*");

            setNextToken();

            ExpressionNode expression = signed_factor();

            multExpression.add(expression, isPositive);

            return term_op(multExpression);
        } else {
            return expr;
        }

    }

    private ExpressionNode signed_factor() {

        if (nextToken.token == Token.PLUSMINUS){
            boolean isPositiv = nextToken.sequence.equals("+");
            setNextToken();
            ExpressionNode expression = factor();

            if (isPositiv){
                return expression;
            } else {
                return new AdditionExpressionNode(expression, false);
            }

        } else {
            return factor();
        }
    }

    private ExpressionNode factor() {

        ExpressionNode expression = argument();
        return factor_op(expression);
    }

    private ExpressionNode factor_op(ExpressionNode expr) {

        if (nextToken.token == Token.RAISED) {
            setNextToken();
            ExpressionNode exponent = signed_factor();

            return new ExponentiationExpressionNode(expr, exponent);
        } else {
            return expr;
        }
    }

    private ExpressionNode argument() {

        if (nextToken.token == Token.FUNCTION) {
            int functionID = stringToFunctionID(nextToken.sequence);
            setNextToken();
            ExpressionNode expression = argument();

            return new FunctionExpressionNode(functionID, expression);
        } else if (nextToken.token == Token.OPEN_B) {
            setNextToken();
            ExpressionNode expression = expression();

            if (nextToken.token != Token.CLOSED_B)
                throw new ParserException("Schluss Klammer erwartet aber " +
                        nextToken.sequence + " gefunden.");

            setNextToken();
            return expression;
        } else {
            return value();
        }


    }

    private ExpressionNode value() {

        if (nextToken.token == Token.NUMBER) {
            ExpressionNode expression = new ConstantExpressionNode(nextToken.sequence);
            setNextToken();

            return expression;
        } else if (nextToken.token == Token.VARIABLE) {
            ExpressionNode expression = new VariableExpressionNode(nextToken.sequence);
            setNextToken();
            return expression;
        }
        if (nextToken.token == Token.EPSILON)
            throw new ParserException("Abrupter abbruch der Zeichenkette");
        else
            throw new ParserException("Unerlaubtes Symbol gefunden: " + nextToken.sequence);


    }
}
