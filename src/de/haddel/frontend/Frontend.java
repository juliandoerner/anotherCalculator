package de.haddel.frontend;

import de.haddel.controller.Controller;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by Doerner_Ju on 04.12.2015.
 */
public class Frontend extends JFrame {

    private Controller controller;

    private JTextField input_txf;
    private JButton enter_btn;
    private JButton a7Button;
    private JButton a8Button;
    private JButton a9Button;
    private JButton a4Button;
    private JButton a5Button;
    private JButton a6Button;
    private JButton a1Button;
    private JButton a2Button;
    private JButton a3Button;
    private JButton a0Button;
    private JButton add_btn;
    private JButton sqrt_btn;
    private JButton pow_btn;
    private JButton sub_btn;
    private JButton div_btn;
    private JButton multi_btn;
    private JButton brackleft_btn;
    private JButton brackright_btn;
    private JPanel mainPanel;
    private JLabel logo;
    private JPanel controler_panel;
    private JPanel enter_panel;
    private JPanel pad_panel;
    private JPanel firstrow_panel;
    private JPanel secondrow_panel;
    private JPanel thirdrow_panel;
    private JPanel zero_panel;
    private JPanel command_panel;
    private JPanel bracket_panel;
    private JButton removeAll_btn;
    private JButton removeLastSing_btn;
    private JLabel copyright;
    private JPanel delete_panel;

    public Frontend(String title, final Controller controller) throws HeadlessException {
        super(title);
        this.controller = controller;

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.setContentPane(mainPanel);
        this.setResizable(false);

        pack();
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);


        setVisible(true);

        controler_panel.setOpaque(false);
        enter_panel.setOpaque(false);
        pad_panel.setOpaque(false);
        firstrow_panel.setOpaque(false);
        secondrow_panel.setOpaque(false);
        thirdrow_panel.setOpaque(false);
        zero_panel.setOpaque(false);
        command_panel.setOpaque(false);
        bracket_panel.setOpaque(false);
        delete_panel.setOpaque(false);


        /** Hier fängt das Chaos an
         *  Wirklich
         */

        //Enter  Start Calculation
        enter_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                controller.startCalculation();
            }
        });

        a0Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input_txf.setText(input_txf.getText() + "0");
            }
        });

        a1Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input_txf.setText(input_txf.getText() + "1");
            }
        });

        a2Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input_txf.setText(input_txf.getText() + "2");
            }
        });

        a3Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input_txf.setText(input_txf.getText() + "3");
            }
        });

        a4Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input_txf.setText(input_txf.getText() + "4");
            }
        });

        a5Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input_txf.setText(input_txf.getText() + "5");
            }
        });

        a6Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input_txf.setText(input_txf.getText() + "6");
            }
        });

        a7Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input_txf.setText(input_txf.getText() + "7");
            }
        });

        a8Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input_txf.setText(input_txf.getText() + "8");
            }
        });

        a9Button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input_txf.setText(input_txf.getText() + "9");
            }
        });

        add_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input_txf.setText(input_txf.getText() + "+");
            }
        });

        sub_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input_txf.setText(input_txf.getText() + "-");
            }
        });

        div_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input_txf.setText(input_txf.getText() + "/");
            }
        });

        multi_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input_txf.setText(input_txf.getText() + "*");
            }
        });

        sqrt_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input_txf.setText(input_txf.getText() + "sqrt(");
            }
        });

        pow_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input_txf.setText(input_txf.getText() + "^");
            }
        });

        brackleft_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input_txf.setText(input_txf.getText() + "(");
            }
        });

        brackright_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input_txf.setText(input_txf.getText() + ")");
            }
        });


        removeAll_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                input_txf.setText("");
            }
        });

        removeLastSing_btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String tmp_String = input_txf.getText();
                input_txf.setText(tmp_String.substring(0, tmp_String.length() - 1));
            }
        });



    }


    public String getInput() {
        return input_txf.getText();
    }

    public void setOutput(String output) {
        input_txf.setText(output);
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here
    }
}
